package Vehiculos;

public class Motor {
	
	private int cilindrada;
	private double potencia;
	private static int maxKm = 300000;
	private int contadorKm = 0;
	
	
	public Motor(int cilindrada, double potencia) {
		this.cilindrada = cilindrada;
		this.potencia = potencia;
	}
	public Motor(double potencia, int cilindrada) {
		this(cilindrada, potencia);
	}
	public Motor() {
		this(1600, 110d);
	}
	public void rodar(int km) {
		if (km < 0) throw new IllegalArgumentException();
		contadorKm += km;
		if (contadorKm > maxKm) throw new IllegalStateException(); 
			
	}
	
	@Override
	public String toString() {
		return "(cilindrada: " + cilindrada + ", potencia: "+ potencia +", contadorKm: " + contadorKm + ")";
	}
	
	public void print() {
		System.out.print(this.toString());
	}
	public void println() {
		System.out.println(this.toString());
	}
}