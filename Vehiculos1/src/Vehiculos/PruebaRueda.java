package Vehiculos;

import Vehiculos.Rueda;

public class PruebaRueda {

	public static void main(String[] args) {
		Rueda rueda = new Rueda("michelin", 80, 60, 40);
		try {
			rueda.rodar(500);
		}
		catch (IllegalArgumentException e) {
			System.out.println("Error: No se admiten numeros negativos.");
		}
		catch (IllegalStateException ee) {
			System.out.println("Error: No puede rodar mas. " + ee.getMessage());
		}
		rueda.pinchar();
		if(rueda.esta_pinchada()) {
			rueda.reparar();
			rueda.println();
		}
		System.out.println("Si pinchada esta en falso signfica que puede rodar");
		System.out.println("Si pinchada esta en true signfica que no puede rodar");
		
		
		
	}

}