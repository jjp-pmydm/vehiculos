package Vehiculos;


public class Rueda extends Motor{
	private String marca;
	private int pulgadas;
	private int anchuraMm;
	private int ratio;
	private static int maxKm = 60000;
	private int rodaduraKm = 0;
	private boolean pinchada = false;
	private boolean cambiar = false;
	
	public Rueda(String marca, int pulgadas, int anchuraMm, int ratio) {
		this.marca = marca;
		this.pulgadas = pulgadas;
		this.anchuraMm = anchuraMm;
		this.ratio = ratio;
	}
	public Rueda() {
		this("", 16, 205, 55);
	}
	
	protected Object clone() throws CloneNotSupportedException {
		Rueda clonRueda = new Rueda(marca, pulgadas, anchuraMm, ratio);
		clonRueda.rodaduraKm = rodaduraKm;
		clonRueda.pinchada = pinchada;
		clonRueda.cambiar = cambiar;
		return clonRueda;
	}
	public void rodar(int km) {
		if (km < 0) throw new IllegalArgumentException();
		if(pinchada) throw new IllegalStateException("La rueda esta pinchada.");
		rodaduraKm += km;
		if (rodaduraKm > maxKm) throw new IllegalStateException("Se ha alcanzado el numero maximo de rodaduras"); 
			
	}
	public void pinchar() {
		pinchada = true;
	}
	public void reparar() {
		pinchada = false;
	}
	public boolean esta_pinchada() {
		return pinchada;
	}
	
	@Override
	public String toString() {
		return "(marca: " + marca + ", pulgadas: "+ pulgadas +", anchuraMm: "
				+ anchuraMm +", ratio: " + ratio + ", rodaduraKm: " + rodaduraKm
				+ " , pinchada: " + pinchada + ",cambiar:" + cambiar + ")";
	}
	public void print() {
		System.out.print(this.toString());
	}
	public void println() {
		System.out.println(this.toString());
	}
}