package Vehiculos;

public class Moto extends Vehiculo {
	private Rueda rueda[] = new Rueda[2];
	
	public Moto(Motor motor, String matricula) {
		super(motor, matricula);
		rueda[0] = new Rueda();
		rueda[1] = new Rueda();
	}

	@Override
	public void reparar() {
		rueda[0].reparar();
		rueda[1].reparar();
	}
	
	@Override
	public void rodar(int km) {
		super.rodar(km);
		rueda[0].rodar(km);
		rueda[1].rodar(km);
	}
	
	@Override
	public String toString() {
		return super.toString() + rueda[0].toString() + rueda[1].toString();
	}

}