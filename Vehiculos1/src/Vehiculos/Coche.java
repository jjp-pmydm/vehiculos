package Vehiculos;

public abstract class Coche {
	protected Motor motor;
	protected String matricula;
	
	public Coche(Motor motor, String matricula) {
		this.motor = motor;
		this.matricula = matricula;
	}
	
	public void rodar(int km) {
		motor.rodar(km);
	}
	public abstract void reparar();
	
	@Override
	public String toString() {
		return "(matricula: " + matricula + ", motor: "+ motor.toString() +")";
	}
	public void print() {	System.out.print(toString());
	}
	public void println() {
		System.out.println(toString());
	}
}